import pandas as pd
import requests, zipfile, io
import os,glob
import shutil
link = 'https://archive.ics.uci.edu/ml/machine-learning-databases/00283/ADL_Dataset.zip'
r = requests.get(link)
z = zipfile.ZipFile(io.BytesIO(r.content))
z.extractall()
folder_path = 'HMP_Dataset'
files =  glob.glob(os.path.join(folder_path, '*/*.txt'), recursive=True)
for filename in files :
    pre, ext = os.path.splitext(filename)
    os.rename(filename,pre+'.csv')
    file_s = pre+'.csv'
a_model = os.listdir(folder_path)
folders = list()
for name in a_model :
    if(name.endswith('MODEL')):
        folders.append(name)
for folder in folders:
    folder_path1 = os.path.join('HMP_Dataset',folder)
    shutil.rmtree(folder_path1)
column_names = ['X_ACCELERATION_UNCALIBRATED','Y_ACCELERATION_UNCALIBRATED','Z_ACCELERATION_UNCALIBRATED','ADL','GENDER','SUBJECT_ID','DATE']
files_csv =  glob.glob(os.path.join(folder_path, '*/*.csv'), recursive=True)
processing_folder = 'Processing_HMP_Dataset'
if not os.path.exists(processing_folder):
    os.mkdir(processing_folder)
i = 0
#count = 0
for filename in files_csv :
    i = i + 1
    list = filename.split("/")
    ADL = list[1].lower()
    last_dash = list[2].rfind('-')
    last_csv = list[2].rfind('.csv')
    gender = list[2][last_dash+1:last_csv-1]
    subject_id = list[2][last_dash+1:last_csv]
    first_dash = list[2].find('-')
    second_last_dash = list[2][:list[2].rfind('-')].rfind('-')
    date = list[2][first_dash+1:second_last_dash]
    raw_data = pd.read_csv(filename, delimiter=' ', header= None, names = column_names)
    #for checking the total count of rows
    #count = count + raw_data.shape[0]
    raw_data['ADL'] = ADL
    raw_data['GENDER'] = gender
    raw_data['SUBJECT_ID'] = subject_id
    raw_data['DATE'] = date
    file_name = 'raw_data' + str(i) +'.csv'
    fullpathname = os.path.join(processing_folder, file_name)
    raw_data.to_csv(fullpathname, index=False)
clean_folder_path = 'Processing_HMP_Dataset'
clean_csv =  glob.glob(os.path.join(clean_folder_path, '*.csv'), recursive=True)
appended_data = []
for per_csv in clean_csv:
    df_individual = pd.read_csv(per_csv, header=0)
    appended_data.append(df_individual)
finalised_data = pd.concat(appended_data)
finalised_data['X_ACCELERATION_UNCALIBRATED'] = finalised_data['X_ACCELERATION_UNCALIBRATED'].apply(lambda x: -14.709+(x/63)*(2*14.709))
finalised_data['Y_ACCELERATION_UNCALIBRATED'] = finalised_data['Y_ACCELERATION_UNCALIBRATED'].apply(lambda y: -14.709+(y/63)*(2*14.709))
finalised_data['Z_ACCELERATION_UNCALIBRATED'] = finalised_data['Z_ACCELERATION_UNCALIBRATED'].apply(lambda z: -14.709+(z/63)*(2*14.709))
finalised_data.to_csv("cleaned_data.csv",index=False)
